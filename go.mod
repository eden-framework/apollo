module gitee.com/eden-framework/apollo

go 1.16

require (
	gitee.com/eden-framework/reflectx v0.0.3
	github.com/sirupsen/logrus v1.9.0
	golang.org/x/sys v0.8.0 // indirect
)
